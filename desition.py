import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder

import pandas as pd 
import numpy as np
# import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


data = pd.read_csv(r'titanic_all.csv')
# data = np.array(data);
# jumlah data dan beserta nama 
# print(data.count());
# menghapus data yang tidak diperlukan dan menyimpan yang diperlukan
data = data.drop(["PassengerId","Name","Sex","SibSp","Parch","Ticket","Fare","Cabin","Embarked"], axis=1)
# menghapus nilai data yang bernilai null atau kosong
data = data.dropna();
data_train = data.drop(["Survived"], axis=1)
# menjadikan survaived menjadi nilai target
data_target = data["Survived"]
# menjadikan 75% data train dan 25% data testing
x_train,x_test,y_train,y_test = train_test_split(data_train,data_target,test_size=0.25)


tree = DecisionTreeClassifier().fit(x_train, np.ravel(y_train))
result = tree.predict(x_test)


acuration = accuracy_score(y_test, result)
print (result)
print(acuration)






# export_graphviz(tree, out_file='tre.dot', class_names=['0','1'], feature_names=data.columns, impurity=False,filled=True)
# import graphviz 
# with.open('tre.dot') as fig:
# 	test = fig.read()
# graphviz.source(test)