import pandas as pd 
import numpy as np
import os
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score


def cleaningdata(data):
	data = pd.read_csv(data)
	# data = np.array(data);
	# jumlah data dan beserta nama 
	# print(data.count());
	# menghapus data yang tidak diperlukan dan menyimpan yang diperlukan
	data = data.drop(["PassengerId","Name","Sex","SibSp","Parch","Ticket","Fare","Cabin","Embarked"], axis=1)
	# menghapus nilai data yang bernilai null atau kosong
	data = data.dropna();
	return data

def desisiontre(x_train,x_test,y_train,y_test , tulis=0):
	tree = DecisionTreeClassifier().fit(x_train, np.ravel(y_train))
	result = tree.predict(x_test)
	acuration = accuracy_score(y_test, result)
	if tulis == 1:
		print(result)
		print("dengan persentase akurasi {}%" .format(result*100))
	return acuration

def kknneighbors(x_train,x_test,y_train,y_test , tulis=0):
	knn = KNeighborsClassifier().fit(x_train, np.ravel(y_train))
	result = knn.predict(x_test)
	acuration = accuracy_score(y_test, result)
	if tulis == 1:
		print(result)
		print("dengan persentase akurasi {}%" .format(result*100))
	return acuration

def nbayes(x_train,x_test,y_train,y_test, tulis=0):
	bayes = GaussianNB().fit(x_train, np.ravel(y_train))
	result = bayes.predict(x_test)
	acuration = accuracy_score(y_test, result)
	if tulis == 1:
		print(result)
		print("dengan persentase akurasi {}%" .format(result*100))
	return acuration

def run():
	run = int(input("lanjut ? 1/0 : "))
	if run == 0:
		print("terimakasih telah menggunakan aplikasi saya\nSalam Hangat Dari Saya\nTRIO AGUNG PURWANTO")
	return run

data = cleaningdata("titanic_all.csv")
data_train = data.drop(["Survived"], axis=1)
# menjadikan survaived menjadi nilai target
data_target = data["Survived"]
# menjadikan 75% data train dan 25% data testing
x_train,x_test,y_train,y_test = train_test_split(data_train,data_target,test_size=0.25 , random_state=42)
jalan = 1

while jalan != 0:
	os.system("clear")
	print("silamat datang di aplikasi saya\nperkenalkan saya agung TI-D 2019\nsilahkan memilih ingin menggunakan metode apa\n1. menggunakan naivebayes\n2. menggunakan knn\n3. menggunakan desisisen tree\n4. membandingkan semua metode\n0. keluar")
	masukan = int(input("masukan pilihan : "))
	if masukan == 1:
		kknneighbors(x_train,x_test,y_train,y_test, 1)
		jalan = run()
	elif masukan == 2:
		nbayes(x_train,x_test,y_train,y_test, 1)		
		jalan = run()
	elif masukan == 3:
		desisiontre(x_train,x_test,y_train,y_test, 1)
		jalan = run()
	elif masukan == 4:
		kkn = kknneighbors(x_train,x_test,y_train,y_test)
		nb = nbayes(x_train,x_test,y_train,y_test)
		ds = desisiontre(x_train,x_test,y_train,y_test)
		result = max(kkn , nb , ds)
		if result == kkn :
			print("KNN Mendapatkan Nilai Pertama Dalam Perbandingan ini")
			print("dengan persentase akurasi {}%" .format(result*100))
		if result == nb :
			print("Naivebayes Mendapatkan Nilai Pertama Dalam Perbandingan ini")
			print("dengan persentase akurasi {}%" .format(result*100))
		if result == ds :
			print("Desision tree Mendapatkan Nilai Pertama Dalam Perbandingan ini")
			print("dengan persentase akurasi {}%" .format(result*100))		
		jalan = run()